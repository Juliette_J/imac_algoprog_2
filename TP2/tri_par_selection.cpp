#include <iostream>
using namespace std;

int tab[]={0,6,4,9,5,7};
int taille=sizeof(tab)/sizeof(tab[0]);

int cherche_min(int tab[], int taille, int indice);
void tri_par_selec(int tab[], int taille);
void affiche_tab(int tab[], int taille);


int main () {
    tri_par_selec(tab, taille);
    affiche_tab(tab, taille);

    return 0;
}


int cherche_min(int tab[], int taille, int indice) {
    int min=tab[indice];
    int id_min=indice;

    for(int i=indice; i<taille ; i++) {
        if(tab[i]<min) {
            min=tab[i];
            id_min=i;
        }
    }
    return id_min;
}

void tri_par_selec(int tab[], int taille) {
    int min=tab[0];
    int id_min=0;
    int temp;

    for(int i=0; i<taille; i++) {
        id_min=cherche_min(tab, taille, i);
        cout << id_min << endl;

        if(id_min!=i) {
            temp=tab[i];
            tab[i]=tab[id_min];
            tab[id_min]=temp;
        }
    }
}

void affiche_tab(int tab[], int taille) {
    for(int i=0; i<taille; i++) {
        cout << tab[i];
    }
}
