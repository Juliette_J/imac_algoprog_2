#include <iostream>
using namespace std;

int tab[]={0,6,4,9,5,7};
int taille=sizeof(tab)/sizeof(tab[0]);

void affiche_tab(int tab[], int taille);
int diviser(int tab[], int ind_debut, int ind_fin);
void tri_rapide(int tab[],int ind_debut, int ind_fin);

// A REVOIR
int main () {

    // Recursif
    tri_rapide(tab,0,taille-1);
    affiche_tab(tab,taille);

    return 0;
}

int diviser(int tab[], int ind_debut, int ind_fin) {
    int ind_av_pivot = ind_debut;
    int temp;

    for(int i=ind_debut+1; i<=ind_fin; i++) {
        if(tab[i]<=tab[ind_debut]) {
            temp=tab[ind_av_pivot];
            tab[ind_av_pivot+1]= tab[i];
            tab[i]=temp; // Echange pour futur placement du pivot
            ind_av_pivot++;
        }
    }

    temp=tab[ind_av_pivot];
    tab[ind_av_pivot] = tab[ind_debut]; // Placement du pivot
    tab[ind_debut]= temp;

    return ind_av_pivot;
}

void tri_rapide(int tab[],int ind_debut, int ind_fin) {
    if(ind_debut<ind_fin) {
        int ind_pivot=diviser(tab, ind_debut, ind_fin); // Division en 2 tableaux
        tri_rapide(tab, ind_debut, ind_pivot-1); // Tri des lowers
        tri_rapide(tab, ind_pivot-1, ind_fin); // Tri des greaters
    }
}

void affiche_tab(int tab[], int taille) {
    for(int i=0; i<taille; i++) {
        cout << tab[i];
    }
    cout << endl;
}
