#include <iostream>
using namespace std;

int tab[]={0,6,4,9,5,7};
int taille=sizeof(tab)/sizeof(tab[0]);

void affiche_tab(int tab[], int taille);
void tri_bulle(int tab[], int taille);

int main () {

    tri_bulle(tab, taille);
    affiche_tab(tab,taille);

    return 0;
}

void tri_bulle(int tab[], int taille) {
    int temp;
    for(int j=0; j<taille-1; j++) {
        for (int i=0; i<taille-1; i++) {
            if(tab[i]>tab[i+1]) {
                temp=tab[i+1];
                tab[i+1]=tab[i];
                tab[i]=temp;
            }
        }
    }
}

void affiche_tab(int tab[], int taille) {
    for(int i=0; i<taille; i++) {
        cout << tab[i];
    }
    cout << endl;
}
