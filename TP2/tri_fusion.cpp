#include <iostream>
using namespace std;

int tab[]={0,6,4,9,5,7};
int taille=sizeof(tab)/sizeof(tab[0]);

void affiche_tab(int tab[], int taille);
int* diviser(int tab[], int taille);
int* fusionner(int t1[], int t2[]);
int* tri_fusion(int tab[]);

// A REVOIR
int main () {

    tri_fusion(tab);
    affiche_tab(tab,taille);

    return 0;
}

int* diviser(int tab[], int taille) {
    int half=(int) taille/2;
    int t1[half];
    int t2[half];

    for(int i=0; i<half; i++) {
        t1[i]=tab[i];
        t2[i]=tab[half+i];
    }

    int* resp[]={t1,t2};

    return *resp;
}

int* fusionner(int t1[], int t2[]) {
    int taille1=sizeof(t1)/sizeof(t1[0]);
    int taille2=sizeof(t2)/sizeof(t2[0]);

    int tab_fus[taille1+taille2];

    int i=0; // Compteur d'indice de tab_fus
    int i1=0; // Compteur pour t1
    int i2=0; // Compteur pour t2

    while(i1<taille1 && i2<taille2) {
        if(t1[i1]<t2[i2]) {
            tab_fus[i]=t1[i1];
            i++;
            i1++;
        }
        else {
            tab_fus[i]=t2[i2];
            i++;
            i2++;
        }
    }

    if(i1==taille1) {
        for(int j=i2; j<taille2; j++) {
            tab_fus[j]=t2[i2];
            i2++;
        }
    }
    else if(i2==taille2) {
        for(int j=i1; j<taille1; j++) {
            tab_fus[j]=t1[i1];
            i1++;
        }
    }
}

int* tri_fusion(int tab[]) {
    int size=sizeof(tab)/sizeof(tab[0]);
    int halfsize=(int) size/2;

    if(size<=1) {
        return tab;
    }
    else {
        int *resp=diviser(tab, size);
        int t1[]=resp[0];
        int t2[]=resp[1];
        t1=tri_fusion(t1);
        t2=tri_fusion(t2);
        return fusionner(t1,t2);
    }
}

void affiche_tab(int tab[], int taille) {
    for(int i=0; i<taille; i++) {
        cout << tab[i];
    }
    cout << endl;
}
