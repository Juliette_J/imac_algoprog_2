#include <iostream>
using namespace std;

int tab[]={0,6,4,9,5,7};
int tab2[]={0,8,3,9,5,2};
int taille=sizeof(tab)/sizeof(tab[0]);
int taille2=sizeof(tab2)/sizeof(tab2[0]);

int result[taille2];


void affiche_tab(int tab[], int taille);
void tri_par_inser(int tab[], int taille);
void tri_par_inser2(int tab[], int result[], int taille);


int main () {

    result[0]=tab2[0];

    // En un seul tableau
    tri_par_inser(tab,taille);
    affiche_tab(tab, taille);

    // En deux tableaux
    tri_par_inser2(tab2, result, taille);
    affiche_tab(result,taille);

    return 0;
}


void tri_par_inser(int tab[], int taille) {
    int value=tab[0];
    int count=0;

    for(int i=1;i<taille;i++) {
        value=tab[i];
        count=i-1;

        while(count>=0 && tab[count]>value) {
            tab[count+1]=tab[count];
            count--;
        }
        tab[count+1]=value;
    }
}

void tri_par_inser2(int tab[], int result[], int taille) {
    int value=tab[0];
    int count=0;

    for(int i=1;i<taille;i++) {
        value=tab[i];
        count=i-1;

        while(count>=0 && tab[count]>value) {
            tab[count+1]=tab[count];
            result[count+1]=tab[count+1];

            count--;
        }
        tab[count+1]=value;
        result[count+1]=value;
    }
}

void affiche_tab(int tab[], int taille) {
    for(int i=0; i<taille; i++) {
        cout << tab[i];
    }
    cout << endl;
}
