#include "tp6.h"
#include <QApplication>
#include <time.h>

MainWindow* w = nullptr;

void Graph::buildFromAdjenciesMatrix(int **adjacencies, int nodeCount)
{
	/**
	  * Make a graph from a matrix
	  * first create all nodes, add it to the graph then connect them
	  * this->appendNewNode
	  * this->nodes[i]->appendNewEdge
	  */

	  for(int i=0; i<nodeCount-1; i++) {
        this->appendNewNode(new GraphNode(i));
    }

    for(int i=0; i<nodeCount-1; i++) {
        for(int j=0; j<nodeCount-1; j++) {
            if(adjacencies[i][j]!=0) {
                this->nodes[i]->appendNewEdge(this->nodes[j],adjacencies[i][j]);
            }
        }
    }
}

void Graph::deepTravel(GraphNode *first, GraphNode *nodes[], int &nodesSize, bool visited[])
{
	/**
	  * Fill nodes array by travelling graph starting from first and using recursivity
	  */

	visited[first->value]=true;
    nodes[nodesSize]=first;
    nodesSize++;

    Edge e=first->edges[0];

    while(e != NULL) {
        if(!visited[e.destination->value]) {
            deepTravel(e.destination, nodes, nodesSize, visited);
        }
        e=e.next;
    }

}

void Graph::wideTravel(GraphNode *first, GraphNode *nodes[], int &nodesSize, bool visited[])
{
	/**
	 * Fill nodes array by travelling graph starting from first and using queue
	 * nodeQueue.push(a_node)
	 * nodeQueue.front() -> first node of the queue
	 * nodeQueue.pop() -> remove first node of the queue
	 * nodeQueue.size() -> size of the queue
	 */
	std::queue<GraphNode*> nodeQueue;
	nodeQueue.push(first);
	while(nodeQueue.size()!=0) {
        nodes[nodesSize]=nodeQueue.front();
        nodeQueue.pop();
        nodesSize++;
        visited[nodes[nodesSize]->value]=true;

        Edge* e=first->edges;
        while(e != nullptr) {
            if(!visited[e->destination->value]) {
                nodeQueue.push(e->destination);
            }
            e=e->next;
        }
    }
}

bool Graph::detectCycle(GraphNode *first, bool visited[])
{
	/**
	  Detect if there is cycle when starting from first
	  (the first may not be in the cycle)
	  Think about what's happen when you get an already visited node
	**/
    int compteur=0;

    for(int i=0; i < Graph.size(); i++) {
        if(!visited[first->value]) {
            visited[first->value]=true;
            first=first->edges->next;
        }
        else {
            return true;
        }
    }
    return false;
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 150;
	w = new GraphWindow();
	w->show();

	return a.exec();
}
