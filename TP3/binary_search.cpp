#include <iostream>
using namespace std;

int tab[]={0,2,4,6,7,9};
int taille=sizeof(tab)/sizeof(tab[0]);
int binary_search(int tab[], int n);

int main () {
    binary_search(tab, 3);

    return 0;
}

int binary_search(int tab[], int n) {
    int start=0;
    int end=taille-1;
    int mid;

    while(start<end) {
        mid=(start+end)/2;

        if(n<tab[mid]) {
            end=mid;
        }
        else if(n>tab[mid]) {
            start=mid+1;
        }
        else if (n==tab[mid]){
            cout << "Indice de n dans le tableau : " << mid << endl;
            return mid;
        }

    }
    cout << "Ce nombre n'est pas dans le tableau !" << endl;
    return -1;
}
