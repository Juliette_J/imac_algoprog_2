#include <iostream>
using namespace std;

int tab[]={0,2,2,2,4,6,7,9};
int taille=sizeof(tab)/sizeof(tab[0]);
int binary_search_all(int tab[], int n);

int main () {
    binary_search_all(tab, 2);

    return 0;
}

int binary_search_all(int tab[], int n) {
    int start=0;
    int end=taille-1;
    int mid;
    int ind_min;
    int ind_max;

    while(start<end) {
        mid=(start+end)/2;

        if(n<tab[mid]) {
            end=mid;
        }
        else if(n>tab[mid]) {
            start=mid+1;
        }

        else if (n==tab[mid]){
            ind_min=mid;
            ind_max=mid;

            while(start<end) {
                start=0;
                end=mid;

                mid=(start+end)/2;

                if(n<tab[mid]) {
                    end=mid;
                }
                else if(n>tab[mid]) {
                    start=mid+1;
                }
                else {
                    ind_min=mid;
                }
            }

            while(start<end) {
                start=mid;
                end=taille-1;

                mid=(start+end)/2;

                if(n<tab[mid]) {
                    end=mid;
                }
                else if(n>tab[mid]) {
                    start=mid+1;
                }
                else {
                    ind_max=mid;
                }
            }
            cout << "Indices ou n se trouve dans le tableau : " << ind_min << " a " << ind_max << endl;
            return(ind_min,ind_max);
        }


    }
    cout << "Ce nombre n'est pas dans le tableau !" << endl;
    return -1;
}
