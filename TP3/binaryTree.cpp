#include "mainwindow.h"
#include "tp3.h"
#include <QApplication>
#include <time.h>
#include <stack>
#include <queue>

MainWindow* w = nullptr;
using std::size_t;

struct SearchTreeNode : public Node
{    
    SearchTreeNode* left;
    SearchTreeNode* right;
    int value;

    void initNode(int value)
    {
        this->value=value;
        left=nullptr;
        right=nullptr;
    }

	void insertNumber(int value) {
        if(this->value > value && this->left == nullptr) {
            this->left=new SearchTreeNode(value);
        }
        else if(this->value > value && this->left != nullptr) {
            this->left->insertNumber(value);
        }

        else if(this->value < value && this->right == nullptr) {
            this->right=new SearchTreeNode(value);
        }
        else if(this->value < value && this->right != nullptr) {
            this->right->insertNumber(value);
        }
    }

	uint height() const	{
        // should return the maximum height between left child and
        // right child +1 for itself. If there is no child, return
        // just 1

        uint h_left=0;
        uint h_right=0;

        if (this->left != nullptr) {
            h_left=this->left->height();
        }

        else if (this->right != nullptr) {
            h_right=this->right->height();
        }

        return (std::max(h_left, h_right)+1);
    }

	uint nodesCount() const {
        // should return the sum of nodes within left child and
        // right child +1 for itself. If there is no child, return
        // just 1

        uint sum_left=0;
        uint sum_right=0;

        if (this->left != nullptr) {
            sum_left=this->left->nodesCount();
        }

        else if (this->right != nullptr) {
            sum_right=this->right->height();
        }

        return sum_right+sum_left;
	}

	bool isLeaf() const {
        // return True if the node is a leaf (it has no children)

        if (this->left == nullptr && this->right == nullptr) {
            return true;
        }

        return false;
	}

	void allLeaves(Node* leaves[], uint& leavesCount) {
        // fill leaves array with all leaves of this tree
        if (isLeaf()) {
            leaves[leavesCount]=this->value;
            leavesCount++;
        }
	}

	void inorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with inorder travel
        nodes[nodesCount]=this->left->value;
        nodesCount++;

        nodes[nodesCount]=this->value;
        nodesCount++;

        nodes[nodesCount]=this->right->value;
        nodesCount++;
	}

	void preorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with preorder travel
        nodes[nodesCount]=this->value;
        nodesCount++;
        
        nodes[nodesCount]=this->left->value;
        nodesCount++;
        
        nodes[nodesCount]=this->right->value;
        nodesCount++;
	}

	void postorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with postorder travel
        nodes[nodesCount]=this->left->value;
        nodesCount++;

        nodes[nodesCount]=this->right->value;
        nodesCount++;
        
        nodes[nodesCount]=this->value;
        nodesCount++;
	}

	Node* find(int value) {
        // find the node containing value
        if(this->value==value) {
            return this;
        }
        
		return nullptr;
	}

    void reset()
    {
        if (left != NULL)
            delete left;
        if (right != NULL)
            delete right;
        left = right = NULL;
    }

    SearchTreeNode(int value) : Node(value) {initNode(value);}
    ~SearchTreeNode() {}
    int get_value() const {return value;}
    Node* get_left_child() const {return left;}
    Node* get_right_child() const {return right;}
};

Node* createNode(int value) {
    return new SearchTreeNode(value);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 200;
    w = new BinarySearchTreeWindow<SearchTreeNode>();
	w->show();

	return a.exec();
}
