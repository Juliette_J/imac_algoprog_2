#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChild(int nodeIndex)
{
    return nodeIndex*2+1;
}

int Heap::rightChild(int nodeIndex)
{
    return nodeIndex*2+2;
}

void Heap::insertHeapNode(int heapSize, int value)
{
	// use (*this)[i] or this->get(i) to get a value at index i
	int i = heapSize;
    this->get(i)=value;
    while(i>0 && this->get(i)>this->get((i-1)/2)) {
        this->swap(this->get(i), this->get((i-1)/2));
        i=(i-1)/2;
    }
}

void Heap::heapify(int heapSize, int nodeIndex)
{
	// use (*this)[i] or this->get(i) to get a value at index i
    int i = nodeIndex;
    int largest;

    if(this->get(i) > this->get(i*2+1) && this->get(i) > this->get(i*2+2)) {
        largest=i;
    }
    else if (this->get(i*2+1) > this->get(i) && this->get(i*2+1) > this->get(i*2+2)) {
        largest=i*2+1;
    }
    else {
        largest=i*2+2;
    }

    if(largest!=i) {
        this->swap(this->get(i), this->get(largest));
        heapify(heapSize,largest);
    }
}

void Heap::buildHeap(Array& numbers)
{
    int size = numbers.size();

    for(int i=0; i < size; i++) {
        insertHeapNode(i, numbers[i]);
    }
}

void Heap::heapSort()
{
    int size = this->size();
    for(int i= size-1; i > 0 ; i--) {
        this->swap(this->get(0), this->get(i));
        heapify(i,0);
    }

}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
	w->show();

	return a.exec();
}
