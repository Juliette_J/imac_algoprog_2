#include <iostream>
#include <math.h>
using namespace std;

// Structures
struct vec2 {
    int re;
    int im;
};

// Prototypes
bool mandelbrot (vec2 z, vec2 point, int n);

int main () {
    vec2 z;
    z.re=3;
    z.im=2;

    vec2 p;
    p.re = 1;
    p.im=5;

    mandelbrot(z, p, 6);

}


bool mandelbrot (vec2 z, vec2 point, int n) {
    int re_carre = (z.re)*(z.re) - (z.im)*(z.im);
    int im_carre = 2*(z.re)*(z.im);

    vec2 new_z;
    new_z.re = re_carre + point.re;
    new_z.im = im_carre + point.im;

    int mod_z = pow((new_z.re)*(new_z.re)+(new_z.im)*(new_z.im),1/2);

    if (mod_z > 2) {
        return true;
    }

    else if (n==0) {
        return false;
    }

    else {
        return mandelbrot(new_z, point, n-1);
    }
    
}
