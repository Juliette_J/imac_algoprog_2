#include <iostream>
#include <cstdlib>
using namespace std;

// Structures
struct Liste {
    int nb;
};

struct DynaTab {
    Liste *liste;
    int taille;
    int capacite;
};

// Prototypes
void initialiseTab(DynaTab *tab);
void ajoute (DynaTab *tab, int value);

void pousser_pile(DynaTab *tab, int valeur);
int retirer_pile(DynaTab *tab);


int main () {
    //Initialisation variables
    DynaTab *tab;

    initialiseTab(tab);
    tab->liste[0].nb=3;
    tab->liste[1].nb=7;
    tab->liste[2].nb=2;

    pousser_pile(tab, 1);
    retirer_pile(DynaTab *tab);

    free(tab);

    return 0;
}


void initialiseTab(DynaTab *tab){
    tab->taille=0;
    tab->capacite=20;

    tab->liste = (Liste*) malloc(sizeof(Liste)*(tab->capacite));
    if(tab->liste == nullptr) {
        cout << "ERROR" << endl;
    }
}

void ajoute (DynaTab *tab, int value) {
    Liste *l=new Liste;
    l->nb=value;

    if(tab->taille==tab->capacite) {
        cout << "Plus assez de place. Augmentez la capacite. Entrez la nouvelle capacite (superieur a " << tab->capacite << ") : ";
        cin >> tab->capacite;
        realloc(tab, tab->capacite * sizeof(DynaTab));
        if(tab->liste == nullptr) {
            cout << "ERROR" << endl;
        }
    }

    tab->liste[tab->taille]=*l;
    if(&(tab->liste[tab->taille-1]) == nullptr) {
            cout << "ERROR" << endl;
    }

    tab->taille++;

    delete(l);
}

void pousser_pile(DynaTab *tab,int valeur) {
   ajoute(tab, valeur);
}

int retirer_pile(DynaTab *tab) {
    int temp=tab->liste[tab->taille-1].nb;
    tab->liste[tab->taille-1].nb=0;
    tab->taille--;

    return temp;
}


