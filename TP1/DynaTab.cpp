#include <iostream>
#include <cstdlib>
using namespace std;

// Structures
struct Liste {
    int nb;
};

struct DynaTab {
    Liste *liste;
    int taille;
    int capacite;
};

// Prototypes
void initialiseTab(DynaTab *tab);
void ajoute (DynaTab *tab, int value);
int cherche(DynaTab *tab, int valeur);
int recupere(DynaTab *tab, int n);
void stocke(DynaTab *tab, int n, int valeur);

int main () {
    //Initialisation variables
    DynaTab *tab;
    DynaTab *tab2;

    initialiseTab(tab);
    initialiseTab(tab2);

    ajoute(tab,3);

    tab2->liste[0].nb=3;
    tab2->liste[1].nb=7;
    tab2->liste[2].nb=2;

    cherche(tab2, 2);
    recupere(tab2, 2);
    stocke(tab2, 2, 8);

    free(tab);
    free(tab2);

    return 0;
}


void initialiseTab(DynaTab *tab){
    tab->taille=0;
    tab->capacite=20;

    tab->liste = (Liste*) malloc(sizeof(Liste)*(tab->capacite));
    if(tab->liste == nullptr) {
        cout << "ERROR" << endl;
    }
}

void ajoute (DynaTab *tab, int value) {
    Liste *l=new Liste;
    l->nb=value;

    if(tab->taille==tab->capacite) {
        cout << "Plus assez de place. Augmentez la capacite. Entrez la nouvelle capacite (superieur a " << tab->capacite << ") : ";
        cin >> tab->capacite;
        realloc(tab, tab->capacite * sizeof(DynaTab));
        if(tab->liste == nullptr) {
            cout << "ERROR" << endl;
        }
    }

    tab->liste[tab->taille]=*l;
    if(&(tab->liste[tab->taille]) == nullptr) {
            cout << "ERROR" << endl;
    }

    tab->taille++;

    delete(l);
}

int cherche(DynaTab *tab, int valeur) {
    for(int i=0; i<tab->taille; i++) {
        if(tab->liste[i].nb==valeur) {
            return i;
        }
    }
    return -1;
}

int recupere(DynaTab *tab, int n) {
    if (n>tab->taille) {
        cout << "Il n'y a rien a cette position. Choisisez une position plus petite." << endl;
        return -1;
    }
    else {
        return tab->liste[n-1].nb;
    }
}

void stocke(DynaTab *tab, int n, int valeur) {
   if (n>tab->taille) {
        cout << "Il n'y a rien a cette position." << endl;
        return;
    }
    else {
        tab->liste[n-1].nb=valeur;
    }
}
