#include <iostream>
using namespace std;

int evens[254];
int tab[] = {0, 4, 7, 3, 2};

void allEvens(int evens[], int array[], int evenSize, int arraySize);

int main () {
    allEvens(evens,tab,0,5);
    return 0;
}

void allEvens(int evens[], int array[], int evenSize, int arraySize) {
    if (arraySize==0) {
        return;
    }
    else if(array[arraySize-1]%2==0) {
        evens[evenSize]=array[arraySize-1];
        evenSize +=1;
        allEvens(evens, array, evenSize, arraySize-1);
    }
    else {
        allEvens(evens, array, evenSize, arraySize-1);
    }
}
